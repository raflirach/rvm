import matplotlib.pyplot as plt
import numpy as np
from sklearn_rvm import EMRVC
import warnings
from abc import ABCMeta, abstractmethod

import os
import math
import cv2
import numpy as np
import scipy.linalg
import io
import base64
import glob

from numpy import linalg
from scipy.optimize import minimize
from scipy.special import expit
from sklearn.base import RegressorMixin, BaseEstimator, ClassifierMixin
from sklearn.metrics.pairwise import pairwise_kernels
from sklearn.multiclass import OneVsRestClassifier
from sklearn.utils.validation import check_X_y, check_is_fitted, check_array

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from flask import Flask, request, jsonify, render_template, url_for
from flask import request
from flask_cors import CORS

import simplejson as json
import sys
UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['jpg'])

app = Flask(__name__, template_folder='template', static_url_path='/static')
CORS(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

np.set_printoptions(threshold=sys.maxsize)

def load_images(folder):
    images = np.array([])
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))
        # img = cv2.resize(img, (2480, 3504))
        im_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        im_canny = cv2.Canny(im_gray,75,150)
        retval, threshold = cv2.threshold(im_canny, 127, 1, cv2.THRESH_BINARY)
        moments = cv2.moments(threshold)
        huMoments = cv2.HuMoments(moments)
        for i in range(0,7):
            huMoments[i] = -1* math.copysign(1.0, huMoments[i]) * math.log10(abs(huMoments[i]))
        if huMoments is not None:
            images = np.append([[images]], huMoments)
    images = images.reshape(int(len(images)/7),7)        
    return images

imgdata = load_images('./static/dataset/scan/daun')
imgdata2 = load_images('./static/dataset/scan/batang')

kelas = []
for i in range(len(imgdata)):
    kelas.append(-1)
for i in range(len(imgdata2)):
    kelas.append(1)

train = np.concatenate([imgdata,imgdata2])

model = EMRVC(kernel="rbf", tol=1e-6, threshold_alpha=1e8, 
                alpha_max=1e9, max_iter=5000,
                epsilon=1e-08, verbose=False,gamma='auto')
model.fit(train, kelas)    

@app.route('/')
def home():
    files = glob.glob('static/upload/*.jpg')
    for f in files:
        os.remove(f)

    files = glob.glob('static/biner/*.jpg')
    for f in files:
        os.remove(f)

    files = glob.glob('static/canny/*.jpg')
    for f in files:
        os.remove(f)
    
    files = glob.glob('static/grayscale/*.jpg')
    for f in files:
        os.remove(f)
    return render_template("index.html")

@app.route('/datalatih')
def datalatih():
    gambar = []
    files = glob.glob('static/dataset/scan/daun/*')
    for f in files:
        gambar.append(f)

    gambar2 = []
    files = glob.glob('static/dataset/scan/batang/*')
    for f in files:
        gambar2.append(f)

    gambar3 = []
    files = glob.glob('static/dataset/test/*')
    for f in files:
        gambar3.append(f)

    return render_template("datalatih.html", gambar=gambar,gambar2=gambar2,gambar3=gambar3)

@app.route('/preprocessing')
def preprocessing():
    filename = '0-001.jpg'
    images = np.array([])
    img = cv2.imread(os.path.join('static/dataset/scan/daun/', filename))
    im_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    im_canny = cv2.Canny(im_gray,75,150)
    retval, threshold = cv2.threshold(im_canny, 127, 1, cv2.THRESH_BINARY)
    # save plot
    plt.imsave('static/grayscale/' + filename ,im_gray,cmap='gray')
    plt.imsave('static/canny/' + filename ,im_canny,cmap='gray')
    plt.imsave('static/biner/' + filename ,threshold,cmap='gray')
    moments = cv2.moments(threshold)
    huMoments = cv2.HuMoments(moments)
    for i in range(0,7):
        huMoments[i] = -1* math.copysign(1.0, huMoments[i]) * math.log10(abs(huMoments[i]))
    if huMoments is not None:
        images = np.append([[images]], huMoments)
    images = images.reshape(int(len(images)/7),7)

    return render_template("preprocessing.html",
        filename = filename,
        RGB=img,
        gray=im_gray,
        canny=im_canny,
        biner=threshold,
        fitur=images
    )

@app.route('/pengujian', methods=['GET', 'POST'])
def pengujian():
    files = glob.glob('static/uploads/*.jpg')
    for f in files:
        os.remove(f)
    return render_template('pengujian.html')

@app.route('/akurasi')
def akurasi():
    gambar = []
    files = glob.glob('static/dataset/test/*')
    for f in files:
        gambar.append(f)

    test = load_images('./static/dataset/test')
    prediksi = model.predict(test)
    aktual = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

    from sklearn import metrics
    accuracy = metrics.accuracy_score(aktual,prediksi)
    presisi = metrics.precision_score(aktual,prediksi)
    recall = metrics.recall_score(aktual,prediksi)
    
    return render_template("akurasi.html", 
        gambar = gambar,
        akurasi = accuracy,
        presisi = presisi,
        aktual = aktual,
        recall = recall,
        prediksi = prediksi
        )

@app.route('/hasil', methods=['GET', 'POST'])
def process():
    if 'file' not in request.files:
        return jsonify({"Hasil": "File tidak ada"})
    if 'file' not in request.files:
        returndata = 'No file part'
        return jsonify({'File': returndata})
    file = request.files['file']
    if file.filename == '':
        returndata = 'File Tidak ada'
        return jsonify({'File': returndata})
    if file:
        images = np.array([])
        filename = file.filename
        file.save(os.path.join('static/upload', filename))
        imgtest = cv2.imread(os.path.join('static/upload', filename))
        im_gray = cv2.cvtColor(imgtest,cv2.COLOR_BGR2GRAY)
        # save plot
        # plt.imsave('static/grayscale/' + filename ,im_gray,cmap='gray')
        im_canny = cv2.Canny(im_gray,75,150)
        # save plot
        # plt.imsave('static/canny/' + filename ,im_canny,cmap='gray')
        retval, threshold = cv2.threshold(im_canny, 127, 1, cv2.THRESH_BINARY)
        moments = cv2.moments(threshold)
        huMoments = cv2.HuMoments(moments)
        for i in range(0,7):
            huMoments[i] = -1* math.copysign(1.0, huMoments[i]) * math.log10(abs(huMoments[i]))
        if huMoments is not None:
            images = np.append([[images]], huMoments)
        images = images.reshape(int(len(images)/7),7)

        hasil = model.predict(images)

        if (hasil[0] == -1) :
            hasil = 'adanya perasaan angkuh dan sombong, \nfanatis, \nidealisme'
        else :
            hasil = 'prinsip realita (tertarik pada hal-hal yang nyata), \nmengakui yang tampak nyata(didominir), \nsensitif'
        

    return render_template(
        "hasil.html", 
        hasil=hasil, 
        filename=filename, 
        ) 

if __name__ ==  "__main__":
    app.run(debug=True)